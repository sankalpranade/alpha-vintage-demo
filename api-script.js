function resetFields() {
		document.forms["alpha-demo"].reset();
	}

function callAlphaAPI() {

	var duration = document.getElementById('duration').value;
	var company = document.getElementById('company').value;

	var api_url = 'https://www.alphavantage.co/query?function=TIME_SERIES_'+duration+'&symbol='+company+'&interval=1min&apikey=H2Q9LT2BZBLNV9KN';
	
	$.ajax({
		type: 'GET',
		async: false,
		global:false,
		url: api_url,
		data: {},
		dataType: 'json',
		success: function(data) 
		{ 
			processDataMinMax(data,company);
			processDataWeekly(data);
		},
		error: function() {
			var errorMessage = "Something bad happened with your call";
			printError(errorMessage);
		}
	});

}

function printError(error) {
	var errorMsg = error;
	console.log(errorMsg);	
}

function processDataMinMax(data,company) {
	console.log("inside processDataMinMax");
	var maxValue = null;
	var minValue = null;
	var response = data;
	var highValues = [];
	var lowValues = [];
	var dataToProcess = response['Time Series (Daily)'];

	for (var key in dataToProcess) {
		if (typeof dataToProcess[key] != 'undefined') {
			highValues.push(dataToProcess[key]['2. high']);
			lowValues.push(dataToProcess[key]['3. low']);
		}
		else {
			console.log('Value not found');
		}
	}
	maxValue =  Math.max.apply(Math, highValues);
	minValue =  Math.min.apply(Math, lowValues);
	printDataMinMax(maxValue, minValue, company);
}

function printDataMinMax(maxValue, minValue, company) {
	$(document).ready(function(){
		$('#minmaxValues').append("<p><b>company Name : "+company+"</b></p><p><b>Highest Values : "+maxValue+"</b></p><p><b>Lowest Value : "+minValue+"</b></p>" );
	});	
}

function processDataWeekly(data,company) {
	var response = data;
	var dataToProcess = response['Time Series (Daily)'];
	var days = [];
	days["Monday"] = [];
	days["Tuesday"] = [];
	days["Wednesday"] = [];
	days["Thursday"] = [];
	days["Friday"] = [];
	for(var key in dataToProcess) {
		if (typeof dataToProcess[key] != 'undefined') {
			var date = new Date(key);
			if(date.getDay() == 1) {
				days["Monday"].push(dataToProcess[key]['2. high']);
			}
			else if(date.getDay() == 2) {
				days["Tuesday"].push(dataToProcess[key]['2. high']);
			}
			else if(date.getDay() == 3) {
				days["Wednesday"].push(dataToProcess[key]['2. high']);
			}
			else if(date.getDay() == 4) {
				days["Thursday"].push(dataToProcess[key]['2. high']);
			}
			else if(date.getDay() == 5) {
				days["Friday"].push(dataToProcess[key]['2. high']);
			}

		}
		else {
			console.log('Value not found');
		}	
	}

	var week = [];
	week[0] = getMondayAverage(days["Monday"]);
	week[1] = getTuesdayAverage(days["Tuesday"]);
	week[2] = getWednesdayAverage(days["Wednesday"]);
	week[3] = getThursdayAverage(days["Thursday"]);
	week[4] = getFridayAverage(days["Friday"]);

	printDataWeekly(week);
}

function printDataWeekly(week) {

		var data = week;	
		var weekDays = ["Monday","Tuesday","Wednesday","Thursday","Friday"];
		var i = 0;

		$(document).ready(function(){
			$('#weekly-data-table').append(
				$('<thead><tr><th>Day</th><th>Average Value</th></tr></thead><tbody></tbody>')
				);

			$.each(data, function(i,value){
				var str = value.toString().split('.');
				var res = str[1].slice(0, 2);
				var rawValue = str[0]+'.'+res;
				var value = parseFloat(rawValue);
				$('#weekly-data-table > tbody:last').append('<tr><td>'+weekDays[i]+'</td><td>'+value+'</td></tr>');
				i++;
			});
		});	

	}

	function getMondayAverage(Monday) {
		var average = findAverage(Monday);
		return average;
	}

	function getTuesdayAverage(Tuesday) {

		var average = findAverage(Tuesday);
		return average;
	}

	function getWednesdayAverage(Wednesday) {
		var average = findAverage(Wednesday);
		return average;
	}

	function getThursdayAverage(Thursday) {
		var average = findAverage(Thursday);
		return average;
	}

	function getFridayAverage(Friday) {
		var average = findAverage(Friday);
		return average;
	}

	function getSum(total, num) {
		return total + num;
	}	
	function findAverage(Arr) {
		var array = Arr.map(Number);
		var sum =  array.reduce(getSum);
		var avg = sum / array.length;
		return avg;
	}